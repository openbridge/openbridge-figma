OpenBridge Figma
===================
This project is used to file issues on the Figma file.
Please file issues [here](https://gitlab.com/openbridge/openbridge-figma/-/issues)